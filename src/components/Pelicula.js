import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Pelicula.scss";

class Pelicula extends Component {
	constructor() {
		super();
		this.state = {
			peliculas: [],
			dominio: "http://laravel-api-crud.test/"
		};

		this.getData = this.getData.bind(this);
	}

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		let options = {
			method: "GET"
		};

		fetch(this.state.dominio + "/api/peliculas", options)
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.setState({ peliculas: data });
			})
			.catch(error => {
				console.log("Hubo un problema con la petición Fetch: " + error.message);
			});
	};

	truncateText = text => {
		let length = 150;

		if (text.length > length) {
			return text.substring(0, text.lastIndexOf(" ", length)) + "...";
		} else {
			return text;
		}
	};

	convertToSlug = text => {
		return text
			.toLowerCase()
			.replace(/ /g, "-")
			.replace(/[^\w-]+/g, "");
	};

	render() {
		const { peliculas } = this.state;

		return (
			<div className="container">
				<div className="nav">
					<Link
						to={{
							pathname: `/agregar`
						}}
					>
						Agregar
					</Link>
				</div>
				<div className="lista row">
					{peliculas.map(pelicula => (
						<div
							key={pelicula.id}
							className="pelicula col-12 col-md-4 col-xl-4"
						>
							<div className="content">
								<img
									src={
										pelicula.imagen_sd
											? pelicula.imagen_sd
											: "https://placeimg.com/509/755/arch/grayscale"
									}
									alt={pelicula.nombre}
								/>

								<div className="info animation-center">
									<h1>{pelicula.nombre}</h1>
									<p>{this.truncateText(pelicula.descripcion)}</p>
									<Link
										to={{
											pathname: `/${this.convertToSlug(pelicula.nombre)}`,
											state: { pelicula: pelicula }
										}}
									>
										Leer más
									</Link>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		);
	}
}

export default Pelicula;
