import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import "./Detalles.scss";

class Detalles extends Component {
	constructor(props) {
		super();
		this.state = {
			pelicula: props.location.state.pelicula,
			fireRedirect: false,
			dominio: "http://laravel-api-crud.test/"
		};
	}

	eliminarPelicula = () => {
		let options = {
			method: "DELETE"
		};

		fetch(
			this.state.dominio + "/api/peliculas/" + this.state.pelicula.id,
			options
		)
			.then(response => {
				if (response.ok) {
					console.log("La película fue eliminada correctamente");
				}
			})
			.then(() => {
				this.setState({ fireRedirect: true });
			})
			.catch(error => {
				console.log("Hubo un problema con la petición Fetch: " + error.message);
			});
	};

	render() {
		const { pelicula } = this.state;
		const { from } = this.props.location.state || "/";
		const { fireRedirect } = this.state;

		console.log(pelicula);
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="hero"></div>
				</div>
				<div className="row">
					<div className="container">
						<div className="row nav">
							<Link to={"/"}>Volver</Link>
							<Link
								to={{
									pathname: `/actualizar`,
									state: { pelicula: pelicula }
								}}
							>
								Actualizar
							</Link>
							<button onClick={this.eliminarPelicula}>Eliminar</button>
						</div>
						<div className="row detalles">
							<div className="col-12 col-lg-5">
								<img
									src={
										pelicula.imagen_hd
											? pelicula.imagen_hd
											: "https://placeimg.com/1012/1500/arch/grayscale"
									}
									alt={pelicula.nombre}
								/>
							</div>
							<div className="col-12 col-lg-6 offset-lg-1">
								<h1>{pelicula.nombre}</h1>
								<h3>Año de lanzamiento</h3>
								<span>{pelicula.anio_lanzamiento}</span>
								<h3>Género</h3>
								<span> {pelicula.generos["nombre"]}</span>
								<h3>Sinopsis</h3>
								<p>{pelicula.descripcion}</p>
							</div>
						</div>
					</div>
				</div>

				{fireRedirect && <Redirect to={from || "/"} />}
			</div>
		);
	}
}

export default Detalles;
