import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import "./Agregar.scss";

class Agregar extends Component {
	constructor() {
		super();
		this.state = {
			generos: [],
			fireRedirect: false,
			dominio: "http://laravel-api-crud.test/"
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		let options = {
			method: "GET"
		};

		fetch(this.state.dominio + "/api/generos", options)
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.setState({ generos: data });
			})
			.catch(error => {
				console.log("Hubo un problema con la petición Fetch: " + error.message);
			});
	};

	buildURLQuery = obj =>
		Object.entries(obj)
			.map(pair => pair.map(encodeURIComponent).join("="))
			.join("&");

	submitHandler = event => {
		event.preventDefault();

		let pelicula = {
			nombre: event.target[0].value,
			anio_lanzamiento: event.target[1].value,
			genero_id: event.target[2].value,
			imagen_sd: event.target[3].value,
			imagen_hd: event.target[4].value,
			descripcion: event.target[5].value
		};

		let options = {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			}
		};

		fetch(
			this.state.dominio + "/api/peliculas?" + this.buildURLQuery(pelicula),
			options
		)
			.then(response => {
				if (response.ok) {
					console.log("La película fue insertada correctamente");
				}
			})
			.then(() => {
				this.setState({ fireRedirect: true });
			})
			.catch(error => {
				console.log("Hubo un problema con la petición Fetch: " + error.message);
			});
	};

	render() {
		const { generos } = this.state;
		const { from } = this.props.location.state || "/";
		const { fireRedirect } = this.state;

		return (
			<div className="container">
				<div className="formulario row">
					<form onSubmit={this.submitHandler}>
						<label htmlFor="nombre">Nombre</label>
						<input type="text" name="nombre" required />

						<label htmlFor="anio_lanzamiento">Año de Lanzamiento</label>
						<input type="number" name="anio_lanzamiento" required />

						<label htmlFor="genero_id">Género</label>
						<select name="genero_id" id="genero_id">
							{generos.map(genero => (
								<option key={genero.id} value={genero.id}>
									{genero.nombre}
								</option>
							))}
						</select>

						<label htmlFor="imagen_sd">Imagen pequeña</label>
						<input type="text" name="imagen_sd" />

						<label htmlFor="imagen_hd">Imagen grande</label>
						<input type="text" name="imagen_hd" />

						<label htmlFor="descripcion">Descripción</label>
						<textarea type="text" name="descripcion" required />

						<input type="submit" value="Enviar" />
					</form>

					{fireRedirect && <Redirect to={from || "/"} />}
				</div>
			</div>
		);
	}
}

export default Agregar;
